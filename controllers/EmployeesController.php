<?php

include_once ROOT . '/models/Employees.php';
include_once ROOT . '/models/User.php';
include_once ROOT . '/models/Pagination.php';

class EmployeesController
{
    /**
     * Operation with employees
     * @param int $page
     * @return bool
     */

    public function actionView($page = 1)
    {
        $userId = User::checkLogged();

        $pages = '';

        if (isset($_POST['submit'])) {
            $pages = $_POST['pages'];
        } else {
            $pages = 20;
        }

        $total = Employees::getTotalEmployees();

        $employees = array();
        $employees = Employees::getEmployees($page);

        $pagination = new Pagination($total, $page, $pages, 'page-');

        $positionList = array();
        $positionList = Employees::getPositionList();

        $departmentList = array();
        $departmentList = Employees::getDepartmentList();

        $rateList = array();
        $rateList = Employees::getRateList();

        require_once(ROOT . '/views/employees/view.php');

        return true;
    }
}