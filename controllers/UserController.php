<?php

include_once ROOT . '/models/User.php';

class UserController
{
    /**
     * Check login
     * @return bool
     */

    public function actionLogin()
    {
        $email = '';
        $password = '';

        if (isset($_POST['submit'])) {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }
            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }

            $userId = User::checkUserData($email, $password);

            if ($userId == false) {
                $errors[] = 'Неправильные данные для входа на сайт';
            } else {
                User::saveUserInSession($userId);
                header("Location: /company/page-1/");
            }
        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }
}