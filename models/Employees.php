<?php

class Employees
{
    /**
     * List of employees (filter depends)
     * @param int $page
     * @return array of employees
     */

    public static function getEmployees($page = 1)
    {
        $pages = '';
        $sort = '';
        $dep = '';
        $rate = '';

        if (isset($_POST['submit'])) {
            $pages = $_POST['pages'];
            $sort = $_POST['sort'];
        } else {
            $pages = 20;
            $sort = 'birthday';
        }

        if (!isset($_POST['department'])) {
            $department = 'IS NOT NULL';
        } else {
            $department = $_POST['department'];
        }

        if (!isset($_POST['position'])) {
            $position = 'IS NOT NULL';
        } else {
            $position = $_POST['position'];
        }

        if (!isset($_POST['rate'])) {
            $rate = 'IS NOT NULL';
        } else {
            $rate = $_POST['rate'];
        }


        $offset = ($page - 1) * $pages;

        $db = Db::getConnection();

        $Employees = array();

        $result = $db->query('
          SELECT e.name, e.birthday, e.total, d.department_title, p.position_title, r.rate_title
          FROM department d 
          JOIN employees e ON d.id=e.department_id
          JOIN position p ON p.id=e.position_id
          JOIN rate r ON r.id=e.rate_id 
          WHERE e.department_id ' . $department .
            ' AND e.position_id ' . $position .
            ' AND e.rate_id ' . $rate .
            ' ORDER BY ' . $sort .
            ' ASC LIMIT ' . $pages .
            ' OFFSET ' . $offset);

        $i = 0;
        while ($row = $result->fetch()) {

            $Employees[$i]['name'] = $row['name'];
            $Employees[$i]['birthday'] = $row['birthday'];
            $Employees[$i]['total'] = $row['total'];
            $Employees[$i]['department_title'] = $row['department_title'];
            $Employees[$i]['position_title'] = $row['position_title'];
            $Employees[$i]['rate_title'] = $row['rate_title'];
            $i++;
        }
        return $Employees;
    }

    /**
     * Names of departments
     * @return array
     */

    public static function getDepartmentList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, department_title FROM department');

        $i = 0;
        $DepartmentList = array();
        while ($row = $result->fetch()) {
            $DepartmentList[$i]['id'] = $row['id'];
            $DepartmentList[$i]['department_title'] = $row['department_title'];
            $i++;
        }
        return $DepartmentList;
    }

    /**
     * Names of positions
     * @return array
     */

    public static function getPositionList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, position_title FROM position');

        $i = 0;
        $PositionList = array();
        while ($row = $result->fetch()) {
            $PositionList[$i]['id'] = $row['id'];
            $PositionList[$i]['position_title'] = $row['position_title'];
            $i++;
        }
        return $PositionList;
    }

    /**
     * Names of rates
     * @return array
     */

    public static function getRateList()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT id, rate_title FROM rate');

        $i = 0;
        $RateList = array();
        while ($row = $result->fetch()) {
            $RateList[$i]['id'] = $row['id'];
            $RateList[$i]['rate_title'] = $row['rate_title'];
            $i++;
        }
        return $RateList;
    }

    /**
     * Amount of all employees
     * @return mixed
     */

    public static function getTotalEmployees()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM employees ');
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();

        return $row['count'];
    }
}