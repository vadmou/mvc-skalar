<?php

class User
{
    /**
     * Check user email and password
     * @param string $email
     * @param string $password
     * @return bool: integer user id or false
     */

    public static function checkUserData($email, $password)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM user WHERE email = :email AND password = :password';

        $result = $db->prepare($sql);
        $result->bindValue(':email', $email);
        $result->bindValue(':password', $password);
        $result->execute();

        $user = $result->fetch();
        if ($user) {
            return $user['id'];
        }
        return false;
    }

    /**
     * Check password, more than 6 characters
     * @param $password
     * @return bool
     */

    public static function checkPassword($password)
    {
        if (strlen($password) >= 6) {
            return true;
        }
        return false;
    }

    /**
     * Check email
     * @param $email
     * @return bool
     */

    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }

    /**
     * Save user
     * @param $userId
     */

    public static function saveUserInSession($userId)
    {
        session_start();
        $_SESSION['user'] = $userId;
    }

    /**
     * Check user
     * @return mixed
     */

    public static function checkLogged()
    {
        session_start();
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        header("Location: / /");
    }
}