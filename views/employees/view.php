<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Работники компании</title>
    <link rel="stylesheet" type="text/css" href="/template/css/style.css">
</head>
<body>
<div class="layout">
    <div class="sidebar"><h3>Фильтры:</h3>

        <form action="#" method="post" name="Form">
            <h4>Кол-во работников на странице:</h4>
            <input type="radio" name="pages" value="20" checked>20<br>
            <input type="radio" name="pages" value="40">40<br>
            <input type="radio" name="pages" value="50">50<br>
            <input type="radio" name="pages" value="100">100<br>

            <h4>Сортировать по:</h4>
            <input type="radio" name="sort" value="birthday" checked>дате рождения<br>
            <input type="radio" name="sort" value="total">месячному окладу<br>

            <h4>Отдел:</h4>
            <?php foreach ($departmentList as $department): ?>
                <input type="radio" name="department" value="= <?php echo $department['id']; ?>">
                <?php echo $department['department_title']; ?> <br>
            <?php endforeach; ?>

            <h4>Должность:</h4>
            <?php foreach ($positionList as $position): ?>
                <input type="radio" name="position" value="= <?php echo $position['id']; ?>">
                <?php echo $position['position_title']; ?> <br>
            <?php endforeach; ?>

            <h4>Форма оплаты:</h4>
            <?php foreach ($rateList as $rate): ?>
                <input type="radio" name="rate" value="= <?php echo $rate['id']; ?>">
                <?php echo $rate['rate_title']; ?> <br>
            <?php endforeach; ?>
            <br>

            <input type="submit" value="Применить фильтр" name="submit">
        </form>
        <br>
    </div>
    <div class="content"><h3>Список работников</h3>
        <table border="1px solid black">
            <tr class="column-name">
                <td>ФИО</td>
                <td>Дата рождения</td>
                <td>Отдел</td>
                <td>Должность</td>
                <td>Форма оплаты</td>
                <td>Оплата за месяц</td>
            </tr>
            <?php foreach ($employees as $employe): ?>
                <tr>
                    <td><?php echo $employe['name']; ?></td>
                    <td><?php echo $employe['birthday'];; ?></td>
                    <td><?php echo $employe['department_title']; ?></td>
                    <td><?php echo $employe['position_title']; ?></td>
                    <td><?php echo $employe['rate_title']; ?></td>
                    <td><?php echo $employe['total']; ?></td>
                </tr>
            <?php endforeach; ?>
        </table>
        <?php echo $pagination->get(); ?>
    </div>
</div>
</body>
</html>