<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Старница регистрации</title>
    <link rel="stylesheet" type="text/css" href="/template/css/style.css">
</head>
<body>
<div class="login-input">
    <?php if (isset($errors) && is_array($errors)): ?>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li> <?php echo $error; ?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>

    <h2>Вход в базу данных</h2>
    <form action="#" method="post">
        <input type="email" name="email" placeholder="E-mail" value="<?php echo $email; ?>"/><br><br>
        <input type="password" name="password" placeholder="Пароль" value="<?php echo $password; ?>"/><br>
        <br><input type="submit" name="submit" value="Вход"/>
    </form>
</div>
</body>
</html>