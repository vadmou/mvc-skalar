-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Янв 16 2017 г., 01:16
-- Версия сервера: 5.7.13
-- Версия PHP: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `skalar`
--

-- --------------------------------------------------------

--
-- Структура таблицы `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL,
  `department_title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `department`
--

INSERT INTO `department` (`id`, `department_title`) VALUES
(1, 'Юридический'),
(2, 'Поставки'),
(3, 'Финансовый'),
(4, 'Служба безопасности'),
(5, 'Управление персоналом');

-- --------------------------------------------------------

--
-- Структура таблицы `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `birthday` date NOT NULL,
  `department_id` int(11) NOT NULL,
  `position_id` int(11) NOT NULL,
  `rate_id` int(11) NOT NULL,
  `hours` int(11) NOT NULL,
  `cost` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `employees`
--

INSERT INTO `employees` (`id`, `name`, `birthday`, `department_id`, `position_id`, `rate_id`, `hours`, `cost`, `total`) VALUES
(1, 'Lawrence Kane', '1982-11-07', 5, 4, 1, 30, 12, 839),
(2, 'Isaiah Levy', '1986-10-26', 5, 5, 2, 34, 8, 616),
(3, 'Orson Barker', '1977-10-01', 5, 1, 2, 31, 19, 988),
(4, 'Kadeem Combs', '1994-06-05', 3, 1, 1, 31, 8, 823),
(5, 'Kamal Moss', '1975-12-27', 4, 4, 1, 38, 12, 977),
(6, 'Drew Campos', '1985-07-22', 1, 1, 2, 36, 19, 837),
(7, 'Lyle Shaw', '1994-12-12', 1, 3, 1, 22, 17, 865),
(8, 'Azalia Hancock', '1991-06-25', 2, 5, 2, 25, 19, 727),
(9, 'Gretchen Knowles', '1984-03-18', 3, 2, 1, 21, 19, 689),
(10, 'Iola Hutchinson', '1987-01-27', 5, 1, 2, 28, 13, 994),
(11, 'Isabelle Holmes', '1975-04-11', 3, 5, 2, 29, 19, 886),
(12, 'Iona Mckenzie', '1990-08-01', 1, 3, 2, 35, 8, 634),
(13, 'Frances Kelly', '1975-10-05', 1, 4, 1, 33, 15, 650),
(14, 'Victoria Hays', '1991-10-11', 5, 3, 1, 38, 17, 762),
(15, 'Hasad Briggs', '1977-07-22', 5, 3, 1, 25, 13, 871),
(16, 'Aquila Whitfield', '1992-04-17', 3, 2, 1, 29, 16, 932),
(17, 'Chaim Prince', '1990-12-05', 5, 5, 2, 29, 8, 969),
(18, 'Mona Bowers', '1977-12-29', 4, 3, 1, 39, 8, 726),
(19, 'Ruth Burks', '1982-01-17', 4, 4, 1, 30, 8, 894),
(20, 'Maris Christian', '1978-12-03', 2, 1, 1, 38, 9, 800),
(21, 'Amber Atkinson', '1976-04-26', 4, 3, 1, 21, 11, 962),
(22, 'Zeus Vinson', '1984-10-26', 4, 1, 2, 28, 11, 843),
(23, 'Gabriel Turner', '1993-12-04', 3, 5, 1, 23, 14, 764),
(24, 'Steel Hurst', '1991-09-19', 2, 3, 1, 29, 10, 989),
(25, 'Alexa Lawson', '1984-08-18', 3, 3, 1, 35, 19, 876),
(26, 'Gage Russell', '1977-02-06', 3, 3, 2, 40, 10, 610),
(27, 'Lillian Goodman', '1980-07-13', 1, 1, 2, 24, 10, 663),
(28, 'Meghan Knox', '1981-10-16', 1, 2, 2, 21, 17, 646),
(29, 'Alexander Hudson', '1981-03-15', 4, 1, 1, 29, 17, 980),
(30, 'Megan Jackson', '1989-09-03', 3, 5, 1, 28, 16, 813),
(31, 'Mary Berg', '1992-10-14', 4, 2, 1, 28, 12, 884),
(32, 'Xena Weiss', '1988-11-06', 4, 5, 1, 39, 17, 866),
(33, 'Pearl Stein', '1982-11-13', 4, 5, 1, 30, 9, 981),
(34, 'Yeo Hutchinson', '1977-11-29', 4, 1, 2, 31, 19, 945),
(35, 'Edward Dillard', '1988-02-17', 3, 1, 1, 33, 14, 862),
(36, 'Ocean Parker', '1988-01-04', 1, 5, 2, 36, 12, 842),
(37, 'Kane Fleming', '1982-10-06', 5, 4, 2, 32, 14, 996),
(38, 'Veda Berg', '1977-06-18', 1, 5, 1, 24, 19, 631),
(39, 'Nigel Callahan', '1992-08-17', 2, 2, 1, 34, 10, 969),
(40, 'Shelley Hudson', '1992-01-03', 4, 1, 1, 35, 19, 975),
(41, 'Deirdre Russo', '1984-12-08', 3, 2, 2, 26, 19, 831),
(42, 'May Macias', '1987-02-09', 4, 1, 2, 39, 15, 914),
(43, 'Kimberley Dickson', '1992-03-01', 3, 2, 2, 20, 10, 636),
(44, 'Allistair Meadows', '1977-06-20', 3, 1, 1, 27, 10, 730),
(45, 'Uriah Serrano', '1987-03-17', 2, 2, 1, 26, 18, 996),
(46, 'Daphne Pate', '1976-11-12', 4, 5, 1, 30, 14, 724),
(47, 'Imogene Whitney', '1979-08-26', 4, 4, 2, 22, 14, 639),
(48, 'Trevor Mccall', '1977-11-03', 5, 2, 1, 32, 11, 742),
(49, 'Darius Pugh', '1987-10-31', 1, 4, 1, 27, 12, 926),
(50, 'Solomon Macias', '1990-05-24', 4, 3, 1, 40, 13, 882),
(51, 'Shaine Anthony', '1978-10-31', 2, 5, 1, 30, 16, 942),
(52, 'Cynthia Klein', '1982-10-06', 3, 1, 2, 35, 11, 799),
(53, 'Rebekah Hickman', '1992-12-06', 5, 5, 2, 32, 8, 825),
(54, 'Imani Young', '1982-04-14', 5, 2, 1, 27, 14, 900),
(55, 'Declan Montgomery', '1978-03-03', 3, 2, 1, 35, 19, 908),
(56, 'Marah Gay', '1978-11-23', 1, 4, 1, 22, 20, 660),
(57, 'Anjolie Smith', '1990-02-26', 5, 1, 2, 30, 13, 965),
(58, 'Karen Moss', '1988-08-09', 3, 3, 1, 22, 17, 658),
(59, 'Leandra Salazar', '1976-12-28', 4, 3, 2, 36, 10, 785),
(60, 'Cameran Becker', '1994-09-11', 3, 5, 2, 33, 9, 913),
(61, 'Hayfa Little', '1978-09-27', 5, 3, 2, 31, 14, 654),
(62, 'Chaney Orr', '1975-11-15', 1, 5, 1, 27, 15, 719),
(63, 'Howard Head', '1988-08-24', 4, 4, 2, 29, 12, 624),
(64, 'Kadeem Kelly', '1982-06-01', 4, 1, 1, 38, 16, 673),
(65, 'Angela Dean', '1986-08-04', 4, 4, 2, 40, 17, 718),
(66, 'Channing Warren', '1983-06-07', 4, 3, 1, 36, 20, 857),
(67, 'Yuli Salinas', '1987-04-03', 1, 1, 1, 28, 10, 732),
(68, 'Audra Glover', '1993-04-14', 2, 2, 1, 29, 11, 835),
(69, 'Constance Strong', '1989-11-18', 5, 4, 1, 34, 19, 909),
(70, 'Felicia Schmidt', '1984-12-08', 1, 1, 2, 34, 15, 753),
(71, 'Hunter Dudley', '1986-02-18', 5, 1, 1, 39, 9, 722),
(72, 'Teagan Bean', '1981-08-10', 2, 2, 1, 28, 10, 919),
(73, 'Reuben Mcclain', '1983-03-22', 3, 5, 2, 30, 13, 736),
(74, 'Beau Randall', '1978-09-20', 1, 5, 1, 34, 18, 811),
(75, 'Alyssa Griffith', '1983-04-09', 3, 4, 1, 35, 11, 865),
(76, 'Yardley Hayes', '1984-04-26', 5, 3, 1, 29, 9, 941),
(77, 'Bernard Caldwell', '1986-03-14', 5, 2, 2, 29, 9, 762),
(78, 'Odessa Golden', '1986-05-29', 3, 3, 1, 28, 15, 723),
(79, 'Talon Blankenship', '1986-01-16', 5, 4, 2, 31, 13, 635),
(80, 'Amir Harvey', '1981-02-03', 5, 1, 1, 22, 13, 623),
(81, 'Georgia Juarez', '1989-01-30', 1, 1, 2, 38, 15, 765),
(82, 'Caesar Welch', '1984-10-11', 2, 4, 2, 35, 19, 658),
(83, 'Aladdin Garza', '1993-01-15', 5, 3, 1, 28, 16, 745),
(84, 'Bryar Gibbs', '1986-11-12', 2, 5, 2, 38, 20, 738),
(85, 'Scarlet Bradford', '1975-07-02', 5, 1, 2, 27, 16, 797),
(86, 'Indira Flowers', '1991-03-13', 5, 1, 2, 31, 15, 763),
(87, 'Yetta Colon', '1981-03-12', 1, 3, 1, 25, 10, 903),
(88, 'Karyn Baldwin', '1993-11-19', 4, 5, 1, 36, 14, 689),
(89, 'Cedric Tyler', '1989-08-10', 2, 2, 1, 37, 14, 862),
(90, 'Nehru Hudson', '1979-11-06', 4, 1, 2, 26, 12, 673),
(91, 'Driscoll Vasquez', '1979-10-13', 2, 2, 1, 26, 14, 772),
(92, 'Aubrey Cabrera', '1981-04-26', 2, 2, 2, 35, 19, 830),
(93, 'Denton Frederick', '1993-05-14', 1, 4, 2, 23, 17, 803),
(94, 'Brenda Faulkner', '1985-04-25', 5, 1, 2, 34, 19, 860),
(95, 'Macaulay Joyce', '1994-08-13', 3, 2, 1, 27, 20, 835),
(96, 'Ignacia Rowland', '1978-05-19', 4, 3, 1, 31, 19, 755),
(97, 'Iliana Christensen', '1989-03-02', 4, 3, 1, 28, 16, 625),
(98, 'Jason Hogan', '1986-04-10', 1, 2, 2, 29, 10, 804),
(99, 'Aubrey Shields', '1983-12-11', 5, 1, 1, 34, 18, 791),
(100, 'Edward Bowers', '1989-04-03', 4, 4, 1, 37, 13, 669);

-- --------------------------------------------------------

--
-- Структура таблицы `position`
--

CREATE TABLE IF NOT EXISTS `position` (
  `id` int(11) NOT NULL,
  `position_title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `position`
--

INSERT INTO `position` (`id`, `position_title`) VALUES
(1, 'Стажер'),
(2, 'Инженер'),
(3, 'Эксперт'),
(4, 'Ассистент'),
(5, 'Агент');

-- --------------------------------------------------------

--
-- Структура таблицы `rate`
--

CREATE TABLE IF NOT EXISTS `rate` (
  `id` int(11) NOT NULL,
  `rate_title` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `rate`
--

INSERT INTO `rate` (`id`, `rate_title`) VALUES
(1, 'Ставка'),
(2, 'Почасовая оплата');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`) VALUES
(1, 'admin', 'admin@mail.com', '123456');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_emploees_department` (`department_id`),
  ADD KEY `FK_emploees_position` (`position_id`),
  ADD KEY `FK_emploees_rate` (`rate_id`);

--
-- Индексы таблицы `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `position`
--
ALTER TABLE `position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `FK_emploees_department` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`),
  ADD CONSTRAINT `FK_emploees_position` FOREIGN KEY (`position_id`) REFERENCES `position` (`id`),
  ADD CONSTRAINT `FK_emploees_rate` FOREIGN KEY (`rate_id`) REFERENCES `rate` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
